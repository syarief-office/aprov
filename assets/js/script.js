/* Author:http://www.rainatspace.com

*/

jQuery(document).ready(function(){

	//RESPONSIVE NAV
	jQuery('ul#menu').slicknav({
		label: '',
		duration: 1000
	});

	//SELECT STYLE
	jQuery('.selectpicker').selectpicker();

	//FILESTYLE
	jQuery(":file").filestyle({input: false});

	//Thumbs Grid
	/*Query( '#ri-grid' ).gridrotator({
		rows		: 2,
		columns		: 5,
		animType	: 'fadeInOut',
		animSpeed	: 1000,
		interval	: 3000,
		step		: 1,
		preventClick : false,
		slideshow : true
		//onhover : true,
		w320		: {
			rows	: 2,
			columns	: 5
		},
		w240		: {
			rows	: 2,
			columns	: 5
		}

	});*/
	
	//CAROUSEL SLIDE
	jQuery(".owl-carousel").owlCarousel({
		items : 1,
		lazyLoad : true,
		navigation : true,
		singleItem:true
     //itemsDesktop : false,
      //itemsDesktopSmall : false,
      //itemsTablet: false,
      //itemsMobile : false
	});

	jQuery(".item-dtl-galery img").click(function() {
        var mainImage = jQuery(this).attr("src"); //Find Image Name
        jQuery("#thumb-target img").attr({ src: mainImage});
    });

    jQuery(".imgFill").imgLiquid({fill:true});
	jQuery(".imgNoFill").imgLiquid({fill:false});
});



